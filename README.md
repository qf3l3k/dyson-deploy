# Running a Dyson Protocol node
This is a guide to running a node in the Dyson Protocol.

IMPORTANT: If you want to deploy a validator, comment out or delete the other services in the docker-compose.yml and leave only `chain`. Validator nodes should be firewalled from the general internet and only connect to "Sentry" nodes which do not have private keys and clients can connect to.

See Sentry Node Architecture Overview: https://forum.cosmos.network/t/sentry-node-architecture-overview/454

You should fork this repo because your own setup will be different depending on your own needs. For example, most nodes will not be running the hermes relayer and can comment that service out.


## Requirements
1. Debian 11
2. Install Docker https://docs.docker.com/engine/install/debian/
3. Currently this assumes you have root

```bash
 $ curl -fsSL https://get.docker.com -o get-docker.sh
 $ sh get-docker.sh
```

## Install dependencies

```
$ apt update
$ apt install entr jq make git ufw
```

## Clone this repo

```
$ git clone --recurse-submodules https://gitlab.com/dysonproject/dyson-deploy.git
$ cd dyson-deploy
```


## Quick start

Join the testnet
```
$ make testnet start
```
or join the mainnet
```
$ make reset # if you had already joined the testnet
$ make mainnet start
```

The `chain` service should connect to the mainnet and start validating blocks within a minute. 

The command `make start` will use `entr` to monitor for upgrades and automatically pull the new containers and restart the services.

If you do not use use this command, be sure to have your own monitoring solution for upgrades.

Also note that this will start the other services but they will not be configured, so continue reading...
 
 
## Accessing the `dysond` service

Use `docker compose exec chain bash` to enter the running container.

(Ignore the warnings about blank env vars)

On the host computer enter the running container
```
$ docker compose exec chain bash
```

Now from within the container create your key and create your validator.
```
# dysond keys add <key name>
....

# dysond tx staking create-validator \
  --amount=1dys \
  --pubkey=$(dysond tendermint show-validator) \
  --moniker=<your validator moniker> \
  --chain-id=dyson-mainnet-1 \
  --commission-rate="0.05" \
  --commission-max-rate="0.20" \
  --commission-max-change-rate="0.01" \
  --min-self-delegation="1" \
  --gas="auto" \
  --gas-prices="0.0001dys" \
  --from=<key name> \
  --gas-adjustment 1.5
```

## Run a full serviced node

There are 4 services `chain`, `frontend`, `swag`, and `hermes` in a full node.


- Chain: 
The actual Dyson chain daemon. The ONLY REQUIRED service.
- Frontend: What renders the WSGI sites and serves the Dashbaord
- Swag: Nginx + let's encrypt that serves the chain services and the frontend.
https://docs.linuxserver.io/general/swag
- Hermes: The relayer for IBC packets
https://hermes.informal.systems/





## Full node Prerequisites
1. Own a domain
2. Own a server 16 GB RAM, 6 CPUs, 320GB SSD
3. Make sure you can get Swag working with your domain with wildcard DNS validation, https://hub.docker.com/r/linuxserver/swag

Setting this up is out of scope of this of this readme. Consult the documentation 
of your domain registrar.


### Connect to server

As a suggestion, use autossh and byobu to maintain access to your server, it's not required, but it does help.

```
autossh -M 1234 -t root@your_server byobu
```

## Setup

### Firewall
Set up firewall permissions.

More information on the ports here: https://docs.cosmos.network/main/run-node/run-production#firewall
```
$ ufw status
$ ufw allow ssh
$ ufw default deny incoming
$ ufw default allow outgoing
$ ufw allow 26656/tcp
$ ufw allow http
$ ufw allow https
$ sudo ufw enable
```
Log out and ssh in again to confirm you have access still


### For mainnet

Update .env replace example.com with your domain
```
$ cp .env.example .env
```

Start the server, verify that the chain syncs and validates blocks.
```
$ make mainnet start
```

Exit the logs (the chain will still be running the background) with `ctrl-c` and build the Ping blockexplorer

```
$ make ping
```

## Set up the TLS certs in swag

Hopefully you already verified how linuxserver/swag works with your DNS host from the Prerequisites and you can set the
variables in the `.env` and set the dns credientials in `./swag-config/dns-conf/`


## Run the server
Stop swag and restart the servers to load the new config files

```
$ docker compose stop swag
$ make start
```
