#!/usr/bin/env bash 

# Given the env var RPC_HOST, fetch all the relevent data

set -x
touch .env
NODE_ID=`curl https://$RPC_HOST/status | jq -r '.result.node_info.id'` 

# pull genesis
curl https://$RPC_HOST/genesis | jq '.result.genesis' > chain_home/.dyson/config/genesis.json

# current version deployed
TAG=`curl -s https://$RPC_HOST/abci_info | jq -r '.result.response.version'`
echo "{\"name\": \"$TAG\"}" > chain_home/.dyson/data/upgrade-info.json ;\

CHAIN_ID=`curl https://$RPC_HOST/status | jq -r '.result.node_info.network'` 
sed -i'' '/CHAIN_ID/d' .env
echo "CHAIN_ID=$CHAIN_ID" >> .env

sed -i'' '/NODE_PERSISTENT_PEERS/d' .env
echo "NODE_PERSISTENT_PEERS=$NODE_ID@$RPC_HOST:26656" >> .env

sed -i'' '/NODE_SEEDS/d' .env
echo "NODE_SEEDS=$NODE_ID@$RPC_HOST:26656" >> .env

sed -i'' '/STATESYNC_RPC_SERVERS/d' .env
# STATESYNC_RPC_SERVERS must have two entries, even if they are the same
echo "STATESYNC_RPC_SERVERS=https://$RPC_HOST:443,https://$RPC_HOST:443"  >> .env

J=`curl -s https://$RPC_HOST/commit | jq "{height: .result.signed_header.header.height, hash: .result.signed_header.commit.block_id.hash}"` 

sed -i'' '/STATESYNC_TRUST_HEIGHT/d' .env
echo $J | jq -r '"STATESYNC_TRUST_HEIGHT=\(.height)"' >> .env

sed -i'' '/STATESYNC_TRUST_HASH/d' .env
echo $J | jq -r '"STATESYNC_TRUST_HASH=\"\(.hash)\""' >> .env
