#!/usr/bin/env bash 

set -x
clear

export LAST_TAG=`jq -re ".name" chain_home/.dyson/data/last-upgrade-info.json`
echo "LAST_TAG: $LAST_TAG" 

export TAG=`jq -re ".name" chain_home/.dyson/data/upgrade-info.json`
echo "TAG: ${TAG:?missing upgrade_info.json}" 

if [[ $LAST_TAG != $TAG ]] ;
then
	echo "Exporting chain"
	TAG=$LAST_TAG make export
	echo "Exporting "
	cp chain_home/.dyson/data/upgrade-info.json chain_home/.dyson/data/last-upgrade-info.json 
fi

docker compose up -d
docker compose logs -f --tail 101
