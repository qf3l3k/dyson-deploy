SHELL := /bin/bash

TAG := $(shell jq -re ".name" chain_home/.dyson/data/upgrade-info.json)
export TAG

ensurepaths:
	mkdir -p chain_home/.dyson/data
	mkdir -p chain_home/.dyson/config
	mkdir -p ping-explorer/src/chains/mainnet

setvars:ensurepaths
	$$(set -a; source .env ; envsubst "`printf '$${%s} ' $$(cat .env | cut -d'=' -f1)`" < client.toml.template > chain_home/.dyson/config/client.toml)
	$$(set -a; source .env ; envsubst "`printf '$${%s} ' $$(cat .env | cut -d'=' -f1)`" < config.toml.template > chain_home/.dyson/config/config.toml)
	$$(set -a; source .env ; envsubst "`printf '$${%s} ' $$(cat .env | cut -d'=' -f1)`" < app.toml.template > chain_home/.dyson/config/app.toml)
	$$(set -a; source .env ; envsubst "`printf '$${%s} ' $$(cat .env | cut -d'=' -f1)`" < default.conf.template > swag-config/nginx/site-confs/default.conf)
	$$(set -a; source .env ; envsubst "`printf '$${%s} ' $$(cat .env | cut -d'=' -f1)`" < ping-dyson.json.template > ping-explorer/src/chains/mainnet/dyson.json)

stop:
	docker compose down

export: 
	docker compose stop chain
	docker compose run -T --rm chain make export

restore:
	docker compose run -T --rm chain make restore-latest

reset: 
	docker compose stop chain
	docker compose run -T --rm chain make reset

start: setvars
	if [[ ! -e chain_home/.dyson/data/upgrade-info.json ]] ; \
	then  \
		echo "{\"name\": \"$${TAG:?TAG not set and upgrade-info.json is missing}\"}" > chain_home/.dyson/data/upgrade-info.json ;\
	fi

	echo "chain_home/.dyson/data/upgrade-info.json" | entr -r bash scripts/start.sh

ping: setvars
	docker run --rm -v `pwd`/ping-explorer:/app node:17-slim bash -c "cd /app && yarn && yarn build"

mainnet: ensurepaths
	RPC_HOST='dys-tm.dysonprotocol.com' scripts/setenv.sh

testnet: ensurepaths
	RPC_HOST='tm-dys-testnet.dysonvalidator.com' scripts/setenv.sh
